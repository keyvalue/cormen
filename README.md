# Algorithms

Implementations of some algorithms from _Introduction to Algorithms 3rd Ed._, by
Thomas H. Cormen, et. al.



## Building

We use Tup. [Get it.](http://gittup.org/tup/)
