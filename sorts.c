#include "sorts.h"
#include <stdbool.h>

void insertion_sort(int *input) {
    // Arrays of len 0 or 1 are considered sorted
    if (sizeof(input) < 2)
        return;
    for (int j = 1; j < sizeof(input); j++) {
        int key = input[j];
        int i = j - 1;
        while (i > 0 && input[i] > key) {
            input[i + 1] = input[i];
            i = i - 1;
        }
        input[i + 1] = key;
    }
}

int compare_arrays(int *a, int *b) {
    // if they're not the same length, bail
    if (sizeof(a) != sizeof(b))
        return false;

    for (int i = 0; i < sizeof(a); i++) {
        if (a[i] != b[i])
            return false;
    }
    return true;
}
