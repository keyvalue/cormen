#include "sorts.h"
#include <stdio.h>

int main() {
    int input[] = {5, 4, 3, 2, 1};
    insertion_sort(input);
    int expected[] = {1, 2, 3, 4, 5};
    if (!compare_arrays(input, expected))
        printf("insertion_sort failed big time\n");
}
