#ifndef _SORTS_H

#include <stdbool.h>

void insertion_sort(int *input);

int compare_arrays(int *a, int *b);

#endif